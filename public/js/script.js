jQuery(function($){
    var socket = io.connect();
    var $loginForm = $('#loginForm');
    var $nickError = $('#nickError');
    var $nickBox = $('#nickname');
    var $users = $('#users');
    var $messageForm = $('#chatMessage');
    var $messageBox = $('#message');
    var $chat = $('#chat');
    var sessionid;
    var myLogin;
    var opponentLogin;


    //Statusy gotowości
    var myStatus;
    var opponentStatus;

    var myFields = [];
    var opponentFields = [];

    var myHit = 0;
    var opponentHit = 0;

    var queueGame;

    $loginForm.submit(function(e){
        e.preventDefault();
        myLogin = $nickBox.val();
        socket.emit('new user', $nickBox.val(), function(data){
            if (data == "false2")
                $nickError.html('Kurcze :( <strong>Sorry!</strong> Mamy już 2 mld graczy online i nasz dedyk zaraz zdechnie, poczekaj jak zwolni się miejsce ;)').show();
            else if(data){
                $('.contentLogin').hide();
                $('.contentWindow').show();
                $('.myUserPanel').html(myLogin).show();
            } else if (data == false){
                $nickError.html('Twój nick jest już używany! <strong>Sorry :(</strong> - dasz radę wybrać inny?').show();
            }
        });
        $nickBox.val('');
        setWindow(true);
    });


    //Użytkownicy - narazie niezuwane
    socket.on('usernames', function(data){


        /*
         var html = '';
         for(i=0; i < data.length; i++){
         html += data[i] + '<br/>'
         }
         $users.html(html);
         */
        //Chwilowa przeróbka, na potrzeby wyswietlenia opponenta, domyślnie wszyscy

        var html = '';
        for(i=0; i < data.length; i++){
            if (data[i]!=myLogin){
                html += data[i] + '<br/>'
                opponentLogin = data[i];
            }
        }
        $users.html(html);

    });

    socket.on('connect', function() {
        sessionid = socket.io.engine.id;
        console.log('My id: '+ sessionid);
    });

    $messageForm.submit(function(e){
        e.preventDefault();
        socket.emit('send message', $messageBox.val(), function(data){
            $chat.append('<span class="error">' + data + "</span><br/>");
        });
        $messageBox.val('');
    });


    $('.runGameButton').bind('click',function(){

        socket.emit('game', myFields, function(data){
            //
        });
        myStatus=1;
        if (opponentStatus){
            queueGame = opponentLogin;
        } else{
            queueGame = myLogin;
        }
        console.log(opponentLogin);
        if (myStatus == 1 && opponentStatus == 1){
            setGamerInQueue();
        }

    });


    socket.on('new message', function(data){
        $chat.append('<span class="msg"><strong>' + data.nick + ': </strong>' + data.msg + "</span><br/>");
    });

    socket.on('whisper', function(data){
        $chat.append('<span class="whisper"><strong>' + data.nick + ': </strong>' + data.msg + "</span><br/>");
    });

    socket.on('opponent ships', function(data){
        if (data.nick != myLogin){
            console.log('User: '+data.nick+' wybrał pola: '+data.msg);
            opponentFields = data.msg;
            $chat.append('<span class="msg">Użytkownik: <strong>' + data.nick + '</strong> Jest gotowy do gry!</span><br/>');
            opponentStatus =1;
            if (myStatus){
                queueGame = myLogin;
            } else {
                queueGame = opponentLogin;
            }
            console.log('Moj status: ' + myStatus + ' Status rywala: '+opponentStatus);
            if (myStatus && opponentStatus){
                setGamerInQueue();
            }
        }

    });






    //Logika gry
    function selectFields(){
        var avaliableFields = 4;
        $('.myShip').click(function(){
            if(!$(this).hasClass('selectField')){
                if (avaliableFields > 0){
                    $(this).addClass('selectField');
                    myFields.push($(this).attr('id'));
                    avaliableFields--;
                    console.log('Moje pola: '+ myFields + ' Dostępne pola: ' + avaliableFields);
                    $('.countShip').text(avaliableFields);
                } else{
                    //TODO: ładny popup zamiast alertu
                    alert("Sorki, nie masz więcej okrętów w swojej flocie!");
                }
            } else {
                $(this).removeClass('selectField');
                var index = myFields.indexOf($(this).attr('id'));
                myFields.splice(index,1);
                avaliableFields++;
                $('.countShip').text(avaliableFields);
                console.log('Moje pola: '+ myFields + ' Dostępne pola: ' + avaliableFields);
            }
        });
    }

    function setGamerInQueue(){
        $('#gamerInQueue').html(queueGame);
    }






    $(document).ready(function(){
        selectFields();
    });


    function setWindow(variable){
        if ($('.content').height() < $(window).height()-87 && variable != true)
            $('.content').height($(window).height()-87);
        else
            $('.content').height('auto');

    }
    $(window).load(function(){
        setWindow();
    });
});